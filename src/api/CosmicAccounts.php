<?php

namespace CosmicFramework\API;

class CosmicAccounts {
    public static function AuthenticateUser($conf_key) {
        $json = file_get_contents('https://accounts.cosmic.media/api/fetch/?authconf='.$conf_key);
        $info = json_decode($json, true);

        if($info["success"] == "1") {
            $return_value = [];
            foreach($info as $key => $value) {
                switch($key) {
                    case "success":
                        break;
                    case "error":
                        break;
                    default:
                        if($value !== "") {
                            $return_value[$key] = $value;
                        } 
                        break;
                }
            }
            return $return_value;
        } else {
            return $info["error"];
        }
    }

    public static function AuthRedirect($public_key) {
        header("Location: https://accounts.cosmic.media/auth/?s=" . $public_key);
    }
}